<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 10.10.14
 * Time: 14:53
 */

class Session {
	//получение значение
	public static function getValue($name){
		return (isset($_SESSION[$name]) ? $_SESSION[$name] : null);
	}

	public static function setValue($name, $value){
		if($value){
			$_SESSION[$name] = $value;
		}else{
			unset($_SESSION[$name]);
		}
	}

} 