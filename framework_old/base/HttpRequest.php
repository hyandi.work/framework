<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 01.10.14
 * Time: 17:51
 */




class CHttpRequest {

	//получение значение параметра
	public function getParam($name, $defaultValue = null){
		return isset($_GET[$name]) ? $_GET[$name] : (isset($_POST[$name]) ? $_POST[$name] : $defaultValue);
	}

	public function redirect($url, $statusCode = 302){
		header('Location: '.$url, true, $statusCode);
		exit;
	}

} 