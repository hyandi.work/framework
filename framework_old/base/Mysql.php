<?php

class GlsMysql extends mysqli {
	public $config = array();
	public $sql = '';
	public function __construct($c){
		$this->config = $c;
		parent::mysqli($this->config['host'],$this->config['user'],$this->config['passw'],$this->config['db']);
		parent::set_charset('utf8');
	}
	/**
	 * $sql - строка запроса
	 * $type - типы передаваемых переменных
	 * $param - значение переменных
	 * @return mysqli_result
	 */
	public function query($sql){
		$arg = func_get_args();
		if(isset($arg[2]) && is_array($arg[2])){
			$arg=array_merge($arg,$arg[2]);
			array_splice($arg,2,1);
		}
		$sql = $arg[0];
		if (isset($arg[1])){
			$typeArr = str_split($arg[1]);
			$tempStr = $sql;$resultStr = '';
			for($i = 0; $i < count($typeArr); $i++){
				$resultStr .= strstr($tempStr, '?', true);
				$tempStr = strstr($tempStr, '?');
				$tempStr[0] = ' ';
				if($typeArr[$i] == 's'){
					$resultStr .= '"'.$this->real_escape_string($arg[2+$i]).'"';
				}elseif($typeArr[$i] == 'i'){
					$resultStr .= $arg[2+$i];
				}elseif($typeArr[$i] == 'f'){
					$resultStr .= $arg[2+$i];
				}

			}

			$sql = $resultStr.$tempStr;//str_replace(array('?', '?', '?'), $replaceArr, $sql);
		}

		if(isset($this->config['replace'])){
			$sql = str_replace($this->config['replace']['find'], $this->config['replace']['value'], $sql);
		}
		//echo($sql.'<br>');
		$this->sql = $sql;
		$data=parent::query($sql);
		return $data;
	}

	public function iquery(){
		$args = implode(func_get_args());
		return $this->query($args);
	}

}

class GlsMysqlResult{

	public static function fetch_all($res, $type_result = MYSQL_ASSOC){
		$ret = array();
		while($row = $res->fetch_array($type_result)){
			$ret[] = $row;
		}
		return $ret;
	}
}