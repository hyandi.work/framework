<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 25.11.2014
 * Time: 11:03
 */

class Setting {

	static public function getSetting($id){
		$res = Main::db()->query('select `value` from fxdesk_azura.setting where id = ?', 'i', array($id));
		if($res){
			$res = $res->fetch_assoc();
			return $res['value'];
		}
		return false;
	}

	static  public function addSetting($id, $value){
		Main::db()->query('update fxdesk_azura.setting set `value` = ? where id = ?', 'si', array($value, $id));
	}
} 