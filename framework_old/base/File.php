<?php

class CFile {
	protected $_file = array(
		'css' => array(),
		'script' => array()
	);

	//add script file
	public function addScriptFile($val){
		array_push($this->_file['script'], $val);
	}
	//add css file
	public function addCssFile($val){
		array_push($this->_file['css'], $val);
	}

	//render all files
	public function renderFile(){
		$str = '';
		foreach($this->_file['script'] as $value){
			$str .= '<script type="text/javascript" src="'.$value.'"></script>';
		}
		foreach($this->_file['css'] as $value){
			$str .= '<link rel="stylesheet" href="'.$value.'">';
		}
		echo $str;
	}

	//render web site
	public function render(){
		$url = Main::app()->getUrl();
		$class = ucfirst($url[1]).'Controller';
		if(file_exists(PROTECTED_DIR.'/controller/'.$class.'.php')) {
			require( PROTECTED_DIR . '/controller/' . $class . '.php' );
			$controller = new $class;

			ob_start();
			$controller->run( $url );
		}
	}
}