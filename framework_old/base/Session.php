<?php

class Session {
	//получение значение
	public static function getValue($name){
		return (isset($_SESSION[$name]) ? $_SESSION[$name] : null);
	}

	public static function setValue($name, $value = null){
		if($value){
			$_SESSION[$name] = $value;
		}else{
			unset($_SESSION[$name]);
		}
	}

	public static function clear(){
		session_destroy();
	}

} 