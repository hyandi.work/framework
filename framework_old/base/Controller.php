<?php
class CController {

	protected $file = '';
	protected $template = 'view';
	protected $_layout = 'main';
	protected $action = '';
	protected $controller = '';

	protected $_title;

	//старт
	public function run($url){
		//init
		$action = 'action'.ucfirst($url[2]);
		$param = null;
		$this->action = $url[2];
		$this->controller = $url[1];
		//run
		$this->preAction();
		call_user_func_array(array($this, $action), Main::app()->getActionParams());
		$this->postAction();
	}

	public function render($_data_=null, $echoContent = false, $_return_=false){
		// we use special variable names here to avoid conflict when extracting data
		if(is_array($_data_))
			extract($_data_,EXTR_PREFIX_SAME,'data');
		$path = Main::app()->getUrl();
		$path = '/'.$path[1];
		$str = '';
		//if (file_exists(DIR.$this->template.SLASH.'header.php') && !$echoContent) require_once(DIR.$this->template.SLASH.'header.php');

		if($this->file == '') {
			require(PROTECTED_DIR . '/view' . implode('/', Main::app()->getUrl()) . '.php');
		}else{
			require(PROTECTED_DIR . '/view' . $path.'/'.$this->file . '.php');
		}

		if(!$echoContent) {
			ob_implicit_flush(false);
			//$msg = 'requere('..')';
			$msg = ob_get_clean();
			ob_start();
			$this->renderLayout(array('msg' => $msg));
		}

		//if (file_exists(DIR.$this->template.SLASH.'footer.php') && !$echoContent) require_once(DIR.$this->template.SLASH.'footer.php');

	}

	public function renderStr($msg){

		$this->renderLayout(array('msg' => $msg));

	}

	public function renderEx($file, $_data_=null, $echoContent = false, $_return_=false ){
		$this->file = $file;
		$this->render($_data_, $echoContent, $_return_);
	}

	protected function renderLayout($_data){
		if(is_array($_data))
			extract($_data,EXTR_PREFIX_SAME,'data');
		$path = PROTECTED_DIR . '/view/layout/' . $this->getLayout() . '.php';

		require($path);
	}

	protected function preAction(){

	}

	protected function postAction(){

	}
	//установка шаблона
	public function setLayout($name){
		$this->_layout = $name;
	}
	//получение шаблона
	public function getLayout(){
		return $this->_layout;
	}
	//возврашение результат в JSON
	public function returnJson($status, $msg = ''){
		echo(json_encode(array('status' => $status, 'msg' => $msg)));
		exit;
	}
	public  function returnJsonMsg($msg){
		echo(json_encode($msg));
		exit;
	}
	//title
	public function getTitle(){
		return $this->_title;
	}
	public function setTitle($val){
		$this->_title = $val;
	}

	//добавлять скрипты и css файлы
	public function addScript($val){
		if(is_array($val)){
			foreach ( $val as $v ) {
				Main::app()->getFile()->addScriptFile($v);
			}

		}else {
			Main::app()->getFile()->addScriptFile( $val );
		}
	}
	public function addCss($val){
		Main::app()->getFile()->addCssFile($val);
	}

	public function redirect($url){
		Main::app()->request->redirect($url);
	}

} 