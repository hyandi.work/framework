<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 21.11.2014
 * Time: 15:25
 */

class Lang {

	static public function parseReplaceLang($string = false){

		$ob = $string ? $string : ob_get_contents();
		if (Main::app()->notokens)
			if ($string)
				return $ob;
			else
				return;
		if (preg_match_all('/\<\|\|(.*?)\|\|\>/s', $ob, $tokens)) {

			$tokens = $tokens[0];
			$res = Main::db()->query('SELECT * FROM langs WHERE lang="'. Main::app()->lang. '" AND hash IN ("'. implode('","', $tokens). '")');
			//var_dump($tokens);exit;
			$reps = array();
			$vals = array();
			if(!$res) return;
			$tokens = array_unique($tokens);
			while ($row = $res->fetch_assoc()) {
				$p = array_search($row['hash'], $tokens);
				if ($p !== FALSE)
					unset($tokens[$p]);
				$reps[] = $row['hash'];
				$vals[] = $row['value'] ? $row['value'] : $row['hash'];
			}

			if (sizeof($tokens) > 0) {
				$ins_query = array();
				foreach ($tokens as $tok)
						$ins_query[] = ('("'.$tok.'", "'.Main::app()->lang.'")');
				//var_dump($tokens);exit;
				Main::db()->iquery('INSERT INTO langs (hash, lang) VALUES ',implode(',',$ins_query));
			}
			if ($string)
				return str_replace($reps, $vals, $ob);

			ob_end_clean();
			ob_start();
			echo str_replace($reps, $vals, $ob);
		} else {
			$result = '';
		}
	}
} 