<?php

function shutdownGLS(){
	if(GLS_DEBUG && Main::db()->error){
		ob_end_clean();
		$err = Main::db()->error;
		$err .= "<br>";
		$err .= Main::db()->sql;
		echo $err;exit;
	}
	Lang::parseReplaceLang();

	while (ob_get_level())
		ob_end_flush();
	//global $time;
	//echo microtime() - $time;
}

// пользовательская функция для обработки ошибок
function errorHandler($errno, $errmsg, $filename, $linenum, $vars)
{
	// временная метка возникновения ошибки
	$dt = date("Y-m-d H:i:s (T)");

	// определим ассоциативный массив соответствия всех
	// констант уровней ошибок с их названиями, хотя
	// в действительности мы будем рассматривать только
	// следующие типы: E_WARNING, E_NOTICE, E_USER_ERROR,
	// E_USER_WARNING и E_USER_NOTICE
	$errortype = array (
		E_ERROR              => 'Ошибка',
		E_WARNING            => 'Предупреждение',
		E_PARSE              => 'Ошибка разбора исходного кода',
		E_NOTICE             => 'Уведомление',
		E_CORE_ERROR         => 'Ошибка ядра',
		E_CORE_WARNING       => 'Предупреждение ядра',
		E_COMPILE_ERROR      => 'Ошибка на этапе компиляции',
		E_COMPILE_WARNING    => 'Предупреждение на этапе компиляции',
		E_USER_ERROR         => 'Пользовательская ошибка',
		E_USER_WARNING       => 'Пользовательское предупреждение',
		E_USER_NOTICE        => 'Пользовательское уведомление',
		E_STRICT             => 'Уведомление времени выполнения',
		E_RECOVERABLE_ERROR  => 'Отлавливаемая фатальная ошибка'
	);
	// определим набор типов ошибок, для которых будет сохранен стек переменных
	$user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);
	/*if(GLS_DEBUG){
		ob_end_clean();
		$err = $dt . " ";
		$err .= "№" . $errno;
		$err .= " " . $errortype[$errno];
		$err .= "  " . $errmsg;
		$err .= " in file " . $filename;
		$err .= " line:" . $linenum;

		if (in_array($errno, $user_errors)) {
			$err .= " " . wddx_serialize_value($vars, "Variable");
		}
		$err .= "<br>";
		echo $err;
	}else {*/
		$err = $dt . "\t";
		$err .= "№" . $errno;
		$err .= "\t" . $errortype[$errno];
		$err .= "\t " . $errmsg;
		$err .= "\t in file " . $filename;
		$err .= "\t line:" . $linenum;

		if (in_array($errno, $user_errors)) {
			$err .= "\t" . wddx_serialize_value($vars, "Variable");
		}
		$err .= "\n";

		// сохраняем в протокол ошибок, а если произошла пользовательская критическая ошибка, то отправляем письмо
		error_log($err, 3, PROTECTED_DIR . "/log/error.log");
	//}
}


class CApplication {
	private $_url;

	/**
	 * @var CHttpRequest
	 */
	public $request;

	/*
	 * @var string
	 */
	public $lang = 'ru';

	private $_config;
	public $config;
	private $_action_params = array();
	private $_file;
	public $notokens = false;

	//инициализация
	public function init($config){
		$this->_config = $config;
		if(isset($config['config']))
			$this->config = $config['config'];
		$this->_file = new CFile();

		//обработка подключаемых файлов
		foreach($this->_config['include'] as $val){
			require_once($val.'.php');
		}

		$this->request = new CHttpRequest();
		//$this->session = new CSession();

		register_shutdown_function('shutdownGLS');
		$old_error_handler = set_error_handler("errorHandler");

		//инициализация URL
		if(isset($_SERVER['REQUEST_URI'])){
			//разбор url
			/*
			 * lang / controller / action / param1 / param2 / ... / paramN
			 */
			$this->_url = $_SERVER['REQUEST_URI'];
			$this->_url = explode('?', $this->_url);
			$this->_url = explode('/', $this->_url[0]);
			if(Session::getValue('site_lang'))
				$this->lang = Session::getValue('site_lang');
			//определение языка
			if(array_search($this->_url[1], $this->_config['lang']) !== FALSE){
				$this->lang = $this->_url[1];
				array_splice($this->_url, 0, -2);
				array_splice($this->_url, 0, 0, '');
				Session::setValue('site_lang', $this->lang);
			}
			//if($this->_url[1] == 'css') return;
			//проверка url если ли

			if(count($this->_url) == 2){
				$this->_url[1] = $this->_config['default_controller'];
				array_push($this->_url, 'index');
			}
			//получение параметров

			if ( (count($this->_url) > 3) ) {

				$this->_action_params = array_splice($this->_url, 3);
			}

			$this->_file->render();

		}
	}



	//

	public function getUrl(){
		return  $this->_url;
	}

	public function getActionParams(){
		return $this->_action_params;
	}

	/**
	 * @return CFile
	 */
	public function getFile(){
		return $this->_file;
	}

	public function getLangs(){
		return $this->_config['lang'];
	}
}

function parseReplaceLang($string = false) {
	global $_lang, $db;

	if (!isset($_lang{0}))
		$_lang = 'en';

	$ob = $string ? $string : ob_get_contents();

	if (Main::app()->request->getParam('notokens'))
		if ($string)
			return $ob;
		else
			return;

	if (preg_match_all('/\<\|\|(.*?)\|\|\>/s', $ob, $tokens)) {

		$tokens = $tokens[0];

		$res = Main::db()->iquery('SELECT * FROM langs WHERE lang="',Main::app()->lang,'" AND hash IN ("',implode('","',$tokens),'")');
		$reps = array(); $vals = array();
		while ($row = $res->fetch_assoc()) {
			$p = array_search($row['hash'],$tokens);
			if ($p !== false)
				unset($tokens[$p]);
			$reps[] = $row['hash'];
			$vals[] = $row['value'] ? $row['value'] : $row['hash'];
		}

		/*if (sizeof($tokens) > 0) {
			$res = Main::db()->iquery('SELECT * FROM langs_list');
			$langs = array();
			while ($row = $res->fetch_assoc()) {
				$langs[] = $row['lang'];
			}
			$ins_query = array();
			foreach ($tokens as $tok)
				foreach ($langs as $lang)
					$ins_query[] = ('("'.$tok.'", "'.$lang.'")');
			Main::db()->iquery('INSERT INTO langs (hash, lang) VALUES ',implode(',',$ins_query));
		}*/

		if ($string)
			return str_replace($reps,$vals,$ob);

		ob_end_clean();
		ob_start();
		echo str_replace($reps,$vals,$ob);
	} else {
		$result = '';
	}


}