<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 01.10.14
 * Time: 11:55
 */
/*
 * @package system
 *
 * ucfirst
 */

define('PROTECTED_DIR', DIR.'/protected');
define('FRAMEWORK_DIR', __DIR__);
define('INCLUDE_DIR', PROTECTED_DIR.'/view/include');

class Main {

	private static $_db;
	public static $mt4manager;
	private static $_app;
	public static $classMap=array();

	/*
	 * подключаемые классы
	 */
	private static $_coreClasses = array(
		'CApplication' => '/base/Application.php',
		'CController' => '/base/Controller.php',
		'CHttpRequest' => '/base/HttpRequest.php',
		//'Setting' => '/base/Setting.php',
		'Lang' => '/base/Lang.php',
		'Session' => '/base/Session.php',
		'CFile' => '/base/File.php',
		'GlsMysql' => '/base/Mysql.php',
		'' => '',
	);

	/**
	 * @return GlsMysql
	 */
	public static function db(){
		return self::$_db;
	}

	//инициализация приложения
	public static function init(){
		if (!session_id()) session_start();

		/*foreach (scandir(FRAMEWORK_DIR.'/app/ext/models') as $filename) {
			$path = FRAMEWORK_DIR.'/app/ext/models/'.$filename;
			if(is_dir($path) && $filename !== '.' && $filename !== '..'){
				if(file_exists($path.'/index.php')){
					require_once($path.'/index.php');
				}
			}
			if (is_file($path) && substr($filename,strlen($filename)-4,4)=='.php') {
				require_once($path);
			}
		}*/
		$config = include(PROTECTED_DIR.'/config/main.php');
		self::$_db = new GlsMysql($config['db']);
		self::$_app = new CApplication();
		self::$_app->init($config);
	}

	public static function includeFile($file){
		require_once(FRAMEWORK_DIR.'/base/'.$file);
	}

	public static function autoload($className)
	{
		// use include so that the error PHP file may appear
		if (substr($className, 0, 15) == 'Services_Twilio') {
			return false;
		}
		if(isset(self::$classMap[$className]))
			include(self::$classMap[$className]);
		elseif(isset(self::$_coreClasses[$className]))
			include(FRAMEWORK_DIR.self::$_coreClasses[$className]);
		else {
			include( PROTECTED_DIR . '/model/' . $className .'.php' );
		}

		return true;
	}

	//доступ к приложениею
	/**
	* @return CApplication
	*/
	public static function app(){
		return self::$_app;
	}
}

spl_autoload_register(array('Main','autoload'));
//инициализация системы

function data_return($arr, $truesize = 0) {
	if(Main::app()->request->getParam('datatables') && DATATABLES) {
		$size = sizeof($arr);
		$data = array();
		$st = Main::app()->request->getParam('iDisplayStart') > 0 ? Main::app()->request->getParam('iDisplayStart') : 0;
		if (Main::app()->request->getParam('iDisplayLength')!==NULL) {
			$end = ($st + (Main::app()->request->getParam('iDisplayLength')) > 0 ? $st+Main::app()->request->getParam('iDisplayLength') : 0);
			$end = $end > $size ? $size : $end;
		} else
			$end = $size;

		if (isset($arr[0])){

			for($i=$st;$i<$end;++$i) {
				$d = array();

				foreach ($arr[$i] as $val) {

					$d[] = $val;
				}
				$data[] = $d;
			}
		} else {

		}


		$arr = array(
			'sEcho' => Main::app()->request->getParam('sEcho') ? Main::app()->request->getParam('sEcho') : rand(0,255),
			'iTotalRecords' => $truesize ? $truesize : $size,
			'iDisplayRecords' => Main::app()->request->getParam('iDisplayLength') ? Main::app()->request->getParam('iDisplayLength') : $size,
			'iTotalDisplayRecords' => $size,
			'aaData' => $data
		);
		return json_encode($arr);
	}
	else
		return json_encode($arr);
}
